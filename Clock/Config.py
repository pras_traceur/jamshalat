from GoogleMercatorProjection import LatLng
from PySide2.QtGui import QColor


# LOCATION(S)
# Further radar configuration (zoom, marker location) can be
# completed under the RADAR section
#primary_coordinates = 44.9764016, -93.2486732  # Change to your Lat/Lon
#primary_coordinates = -6.385589, 106.830711 # Depok, West Java, Indonesia
primary_coordinates = -6.175110, 106.865036 #Jakarta
lat = -6.175110
lon = 106.865036
timezone = +7
name = "Masjid Bening Guru Semesta"
address = "Jl Ampera Raya - Jakarta"
phone = "021-12345668"

location = LatLng(primary_coordinates[0], primary_coordinates[1])
primary_location = LatLng(primary_coordinates[0], primary_coordinates[1])
#noaastream = 'http://www.urberg.net:8000/tim273/edina'
noaastream = 'sounds/adzan.mp3'
adzan = 'sounds/adzan.mp3'
adzans ='sounds/adzan_tes.wav'
adzan_fajr = 'sounds/adzan-fajr.mp3'
lafadz = 'image/bismillah.jpg'
background = 'images/clockbackground-kevin.png'
background3 = 'images/clockbackground-kelly.png'
background4 = 'images/bg21.png'#'images/bg8.jpg'
iqamahbg = 'images/bg30.jpg'#'images/bg8.jpg'
newiqamah = 'images/bg13.jpg'
adzanbg = 'images/bg10'
background6 = 'images/bg26.jpg'#'images/bg8.jpg'
background7 = 'images/green.jpg'
background8 = 'images/black.jpg'
background9 = 'images/background.jpeg'
mainclassicbg = 'images/green.png'
praAdzan = 'images/final.jpg'
logo = 'images/sabranglor.png'
beforeAdzan = 'images/pradzan.jpeg'
attention = 'images/attention.png'
slash = 'images/label.png'
#attention = 'images/attention.jpeg'
soundless = 'images/silent.jpeg'
squares1 = 'images/squares1-kevin.png'
squares2 = 'images/squares2-kevin.png'
icons = 'icons-lightblue'
textcolor = '#bef'
textcolor2 = 'white'
textcoloractive = '#fbb'
clockface = 'images/ClockfaceNew.png'
clockfaceOld = 'images/clockface3.png'
hourhand = 'images/hourhand.png'
minhand = 'images/minhand.png'
sechand = 'images/sechand.png'
TextAdzan = 'Sedang adzan harap tenang dan matikan/silent handphone anda'
website = 'www.bgs.co.id'
mainInfo = 'Please ensure your mobile phone is silent in the prayer hall'
textadzantime = 'Mobile silent please!'
hadist = 'Lima shalat yang telah Allah Ta’ala wajibkan kepada para hamba-Nya. Siapa saja yang mendirikannya dan tidak menyia-nyiakan sedikit pun darinya karena meremehkan haknya, maka dia memiliki perjanjian dengan Allah Ta’ala untuk memasukkannya ke dalam surga. Sedangkan siapa saja yang tidak mendirikannya, dia tidak memiliki perjanjian dengan Allah Ta’ala. Jika Allah menghendaki, Dia akan Menyiksanya. Dan jika Allah Menghendaki, Allah akan memasukkan ke dalam surga.'

digital = 1             # 1 = Digtal Clock, 0 = Analog Clock

# Goes with light blue config (like the default one)
digitalcolor = "#50CBEB"
digitalcolor2 = "#F1F3CE" #"#E4EA8E"
digitalcolor3 = "#CBEB50"
digitalcolor4 = "#FFFF33"
digitalcolor5 = "#a4e075"
digitalcolors = "red"
digitalColorCountdownTarhim ="#FFFFFF"
# digitalformat = "{0:%I:%M:%S %p}"  # The format of the time
digitalformat = "{0:%H:%M:%S}"
digitalsize = 150
mainframeClockDigitSize = 50
digitalsize3 = 50
digitalsize4 = 25
digitalsizeContdownTarhim = 75
# The above example shows in this way:
#  https://github.com/n0bel/PiClock/blob/master/Documentation/Digital%20Clock%20v1.jpg
# ( specifications of the time string are documented here:
#  https://docs.python.org/2/library/time.html#time.strftime )

# digitalformat = "{0:%I:%M}"
# digitalsize = 250
#  The above example shows in this way:
#  https://github.com/n0bel/PiClock/blob/master/Documentation/Digital%20Clock%20v2.jpg

usemapbox = 1   # Use Mapbox.com for maps, needs api key (mbapi in ApiKeys.py)
metric = 0  # 0 = English, 1 = Metric
radar_refresh = 10      # minutes
weather_refresh = 30    # minutes
# Wind in degrees instead of cardinal 0 = cardinal, 1 = degrees
wind_degrees = 0

# gives all text additional attributes using QT style notation
# example: fontattr = 'font-weight: bold; '
fontattr = ''

# These are to dim the radar images, if needed.
# see and try Config-Example-Bedside.py
dimcolor = QColor('#000000')
dimcolor.setAlpha(0)

# Language Specific wording
# DarkSky Language code
#  (https://darksky.net/dev/docs under lang=)
Language = "ID"

# The Python Locale for date/time (locale.setlocale)
#  '' for default Pi Setting
# Locales must be installed in your Pi.. to check what is installed
# locale -a
# to install locales
# sudo dpkg-reconfigure locales
DateLocale = 'id_ID.utf-8'
DateLocale2 = 'en_GB.utf-8'

# Language specific wording
LPressure = "Pressure "
LHumidity = "Humidity "
LWind = "Wind "
Lgusting = " gusting "
LFeelslike = "Feels like "
LPrecip1hr = " Precip 1hr:"
LToday = "Today: "
LSunRise = "Sun Rise:"
LSet = " Set: "
LMoonPhase = " Moon Phase:"
LInsideTemp = "Inside Temp "
LRain = " Rain: "
LSnow = " Snow: "
Lmoon1 = 'New Moon'
Lmoon2 = 'Waxing Crescent'
Lmoon3 = 'First Quarter'
Lmoon4 = 'Waxing Gibbous'
Lmoon5 = 'Full Moon'
Lmoon6 = 'Waning Gibbous'
Lmoon7 = 'Third Quarter'
Lmoon8 = 'Waning Crecent'

# RADAR
# By default, primary_location entered will be the
#  center and marker of all radar images.
# To update centers/markers, change radar sections below the desired lat/lon as:
# -FROM-
# primary_location,
# -TO-
# LatLng(44.9764016,-93.2486732),
radar1 = {
    'center': primary_location,  # the center of your radar block
    'zoom': 7,  # this is a maps zoom factor, bigger = smaller area
    'style': 'mapbox/satellite-streets-v10',  # optional style (mapbox only)
    'markers': (   # google maps markers can be overlayed
        {
            'location': primary_location,
            'color': 'red',
            'size': 'small',
            'image': 'teardrop-dot',  # optional image from the markers folder
        },          # dangling comma is on purpose.
    )
}


radar2 = {
    'center': primary_location,
    'zoom': 11,
    'markers': (
        {
            'location': primary_location,
            'color': 'red',
            'size': 'small',
        },
    )
}


radar3 = {
    'center': primary_location,
    'zoom': 7,
    'markers': (
        {
            'location': primary_location,
            'color': 'red',
            'size': 'small',
        },
    )
}

radar4 = {
    'center': primary_location,
    'zoom': 11,
    'markers': (
        {
            'location': primary_location,
            'color': 'red',
            'size': 'small',
        },
    )
}
