# -*- coding: utf-8 -*-                 # NOQA

import sys
import os
import datetime
import time
import json
import locale
import webbrowser
import feedparser

from PySide2 import QtWidgets, QtGui, QtCore, QtNetwork
from PySide2.QtGui import QPixmap, QBrush, QColor
from PySide2.QtGui import QPainter, QImage, QFont
from PySide2.QtCore import QUrl
from PySide2.QtCore import Qt
from PySide2.QtNetwork import QNetworkReply
from PySide2.QtNetwork import QNetworkRequest
from subprocess import Popen
#from pygame import mixer

from lib.Configuration import Configuration
from lib.slideshow import MainSlideFrame, SlideOneFrame, SlideTwoFrame, AdzanFrame, IqamahFrame, ShalatFrame

sys.dont_write_bytecode = True
from praytimes import PrayTimes
from datetime import date

class MainWin(QtWidgets.QWidget):

    def __init__(self, parent):
        QtWidgets.QWidget.__init__(self)
        self.parent = parent

    def keyPressEvent(self, event):
        if isinstance(event, QtGui.QKeyEvent):
            if event.key() == Qt.Key_F4:
                self.parent.myquit()
            if event.key() == Qt.Key_F2:
                self.parent.lastkeytime = time.time() + 2
            if event.key() == Qt.Key_Space:
                self.parent.nextframe(1)
            if event.key() == Qt.Key_Left:
                self.parent.nextframe(-1)
            if event.key() == Qt.Key_Right:
                self.parent.nextframe(1)

    def mousePressEvent(self, event):
        if type(event) == QtGui.QMouseEvent:
            self.parent.nextframe(1)

class SlideshowClock:
    def getJadwalSholat(self):
        import requests 
        import json,ast

        r = requests.get("http://localhost:8000/sholat/native/") 
        data = r.json() 
        times = ast.literal_eval(json.dumps(data))
        return times

    def getKajian(self):

        import requests
        import json,ast

        r = requests.get("http://localhost:8000/kajian/")
        data = r.json() 
        return data

    def tick(self):
        if self.OneSec > 0:
            self.OneSec -= 1
        else:
            self.OneSec = 1
#            if datas == 0:
#                datas = self.getKajian()
#            kajianLength = len(datas)

            self.slideOneFrame.updateKajianDisplay(datas[self.newsindex])
            self.newsindex = (self.newsindex)

        if self.Config.DateLocale != "":
            try:
                locale.setlocale(locale.LC_TIME, 'IND')
            except:
                pass

        now = datetime.datetime.now()
        timestr = self.Config.digitalformat.format(now)
        if self.Config.digitalformat.find("%I") > -1:
            if timestr[0] == '0':
                timestr = timestr[1:99]
        if self.lasttimestr != timestr:
            self.mainSlideFrame.updateClock(timestr)
            self.slideOneFrame.updateClock(timestr)
            self.slideTwoFrame.updateClock(timestr)
            self.adzanFrame.updateClock(timestr)
            self.shalatFrame.updateClock(now)

            current_time_seconds = sum([a*b for a,b in zip(self.seconds_hms, [now.hour, now.minute, now.second])])
            self.time_diff_seconds -= 1

            if self.time_diff_seconds > 0:
                self.insixty = True

            if self.time_diff_seconds == 0 and self.insixty:  
                print("a")              
                self.nextframe(3)
                self.insixty = False
                self.is_adzan = True
                self.time_adzan_seconds = 10 * 60
            
            if self.is_adzan:
                self.time_adzan_seconds -= 1
                
                if self.time_adzan_seconds < 0:
                    self.nextframe(1)
                    self.is_adzan = False
                    self.is_iqamah = True
                    self.time_iqamah_seconds = 1 * 60

            if self.is_iqamah:
                self.time_iqamah_seconds -= 1
                if self.time_iqamah_seconds < 0:
                    self.nextframe(1)
                    self.is_iqamah = False
                    self.is_shalat = True
                    self.time_shalat_seconds = 10 * 60

            if self.is_shalat:
                self.time_shalat_seconds -= 1

                if self.time_shalat_seconds < 0:
                    self.nextframe(1)
                    self.is_shalat = False

            # If time difference is negative, set alarm for next day
            if self.time_diff_seconds < 0:
                self.alarm_index += 1
                if self.alarm_index > 5:
                    self.alarm_index = 0

                self.mainSlideFrame.time_active(self.alarm_index)

                alarm_input = self.data_adzan[self.alarm_index]
                alarm_time = [int(n) for n in alarm_input.split(":")]


                alarm_seconds = sum([a*b for a,b in zip(self.seconds_hms[:len(alarm_time)], alarm_time)])

                self.time_diff_seconds = alarm_seconds - current_time_seconds
                print(self.time_diff_seconds)
                
                if self.adzanplayer is None:
                    if self.alarm_index == 1:
                        mixer.init()
                        print("Adzannnnn shubuh....")
                        mixer.music.load(self.Config.adzan_fajr)
                        mixer.music.play()
                    elif self.alarm_index != 2:
                        mixer.init()
                        print("Adzannnnn....")
                        mixer.music.load(self.Config.adzans)
                        mixer.music.play()
            deltastr = str(datetime.timedelta(seconds=self.time_diff_seconds+630))
            self.iqamahFrame.setCountdown(deltastr)

        self.lasttimestr = timestr



        dy = "{0:%I:%M %p}".format(now)
        tn = "{0:%H:%M:%S}".format(now)
        if dy != self.pdy:
            self.pdy = dy

        # Set format tanggal indonesia, hendra    
        if now.day != self.lastday:
            self.lastday = now.day
            # date
            day = "{0:%A}".format(now)
            ds = "{0.day} {0:%B} {0.year}".format(now)
            from convertdate import islamic

            islamicDate = islamic.from_gregorian(now.year, now.month, now.day)
            print("ISLAMIC DATE")
            print(islamicDate)
            self.mainSlideFrame.updateDate(day, ds, islamicDate)
            self.slideOneFrame.updateDate(day, ds, islamicDate)
            times = self.getJadwalSholat()
            for i in ['Fajr', 'Sunrise', 'Dhuhr', 'Asr', 'Maghrib', 'Isha', 'Midnight']:
                print(i + ': ' + times[i.lower()])

            self.data_adzan = [times[i.lower()] for i in ['Fajr', 'Sunrise', 'Dhuhr', 'Asr', 'Maghrib', 'Isha']]
            self.mainSlideFrame.updateSholatDisplay(self.data_adzan)

    def qtstart(self):
        global ctimer
        global manager
        global datas
        datas = self.getKajian()
        ctimer = QtCore.QTimer()
        ctimer.timeout.connect(self.tick)
        ctimer.start(1000)

    def realquit():
        QtWidgets.QApplication.exit(0)


    def myquit(a=0, b=0):
        global ctimer
        ctimer.stop()
        QtCore.QTimer.singleShot(30, realquit)


    def nextframe(self,plusminus):
        self.frames[self.framep].setVisible(False)
        self.framep += plusminus
        if self.framep >= len(self.frames):
            self.framep = 0
        if self.framep < 0:
            self.framep = len(self.frames) - 1
        self.frames[self.framep].setVisible(True)

    def __init__(self):
        self.Config = Configuration.fetch()
        self.lastday = -1
        self.pdy = ""
        self.lasttimestr = ""
        self.lastkeytime = 0

        app = QtWidgets.QApplication(sys.argv)
        desktop = app.desktop()
        rec = desktop.screenGeometry()
        height = rec.height()
        width = rec.width()

        w = MainWin(self)

        w.setStyleSheet("QWidget { background-color: black;}")

        xscale = float(width) / 1440.0
        yscale = float(height) / 900.0

        self.frames = []
        self.framep = 0


        #Frame utama saat pertama kali tampil
        print("make self.mainSlideFrame")

        self.mainSlideFrame = MainSlideFrame(w, self.Config, width, height)
        #self.mainSlideFrame.setVisible(False)
        self.frames.append(self.mainSlideFrame)
        print("made self.mainSlideFrame")

        #frame countdown waktu tarhim
        self.slideOneFrame = SlideOneFrame(w, self.Config, width, height)
        self.slideOneFrame.setVisible(False)

        self.frames.append(self.slideOneFrame)
        print("made self.slideOneFrame")

        self.slideTwoFrame = SlideTwoFrame(w, self.Config, width, height)
        self.slideTwoFrame.setVisible(False)
        self.frames.append(self.slideTwoFrame)

        #frame saat sedang self.adzan
        print("make self.adzanFrame")
        self.adzanFrame = AdzanFrame(w, self.Config, width, height)
        self.adzanFrame.setVisible(False)
        self.frames.append(self.adzanFrame)
        print("made self.adzanFrame")

        self.iqamahFrame = IqamahFrame(w, self.Config, width, height)
        self.iqamahFrame.setVisible(False)
        self.frames.append(self.iqamahFrame)

        #frame saat sedang melaksanakan shalat
        print("make self.shalatFrame")
        self.shalatFrame = ShalatFrame(w, self.Config, width, height)
        self.shalatFrame.setVisible(False)
        self.frames.append(self.shalatFrame)
        print("made self.shalatFrame")
                
        times = self.getJadwalSholat()

        for i in ['Fajr', 'Sunrise', 'Dhuhr', 'Asr', 'Maghrib', 'Isha', 'Midnight']:
             print(i + ': ' + times[i.lower()])

        self.data_adzan = [times[i.lower()] for i in ['Fajr', 'Sunrise', 'Dhuhr', 'Asr', 'Maghrib', 'Isha']]

        dict_adzan = {
            0 : 'Fajr',
            1 : 'Sunrise',
            2 : 'Dhuhr',
            3 : 'Asr',
            4 : 'Maghrib',
            5 : 'Isha'
        }

        self.seconds_hms = [3600, 60, 1] # Number of seconds in an Hour, Minute, and Second

        # Get the current time of day in seconds
        nowX = datetime.datetime.now()
        current_time_seconds = sum([a*b for a,b in zip(self.seconds_hms, [nowX.hour, nowX.minute, nowX.second])])

        print("Now X " + "{0:%H:%M:%S}".format(nowX))
        print("current_time_seconds " + str(current_time_seconds))

        self.adzanplayer = None

        a_idx = 0
        for i in self.data_adzan:
            i_time = [int(n) for n in i.split(":")]
            i_time_seconds = sum([a*b for a,b in zip(self.seconds_hms, [i_time[0], i_time[1], 0])])
            if i_time_seconds > current_time_seconds:
                print("a_idx " + str(a_idx))
                break
            a_idx += 1

        if a_idx > 5:
            a_idx = 0

        self.alarm_index = a_idx
        print("a_idx after " + str(a_idx))

        self.mainSlideFrame.time_active(self.alarm_index)

        alarm_input = self.data_adzan[self.alarm_index]
        alarm_time = [int(n) for n in alarm_input.split(":")]
        self.insixty = False
        self.is_adzan = False
        self.is_shalat = False
        self.is_iqamah = False
        self.OneSec = 1
        self.newsindex = 0

        # Convert the alarm time from [H:M] or [H:M:S] to seconds
        alarm_seconds = sum([a*b for a,b in zip(self.seconds_hms[:len(alarm_time)], alarm_time)])

        # Calculate the number of seconds until alarm goes off
        self.time_diff_seconds = alarm_seconds - current_time_seconds

        print("self.time_diff_seconds " + str(self.time_diff_seconds))


        # If time difference is negative, set alarm for next day
        if self.time_diff_seconds < 0:
            self.time_diff_seconds = 86400 - current_time_seconds + alarm_seconds # number of seconds in a day

        # Display the amount of time until the alarm goes off
        print("Alarm set to go off in %s" % datetime.timedelta(seconds=self.time_diff_seconds))

        manager = QtNetwork.QNetworkAccessManager()

        stimer = QtCore.QTimer()
        stimer.singleShot(10, self.qtstart)

        w.show()
        w.showFullScreen()

        sys.exit(app.exec_())

a=SlideshowClock()
