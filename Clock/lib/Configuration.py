import sys
import os
import ApiKeys                                              # NOQA
class Configuration:
    def fetch():
        configname = 'Config'

        if len(sys.argv) > 1:
            configname = sys.argv[1]

        if not os.path.isfile(configname + ".py"):
            print("Config file not found %s" % configname + ".py")
            exit(1)

        Config = __import__(configname)

        # define default values for new/optional config variables.

        try:
            Config.location
        except AttributeError:
            Config.location = Config.wulocation

        try:
            Config.metric
        except AttributeError:
            Config.metric = 0

        try:
            Config.weather_refresh
        except AttributeError:
            Config.weather_refresh = 30   # minutes

        try:
            Config.radar_refresh
        except AttributeError:
            Config.radar_refresh = 10    # minutes

        try:
            Config.fontattr
        except AttributeError:
            Config.fontattr = ''

        try:
            Config.dimcolor
        except AttributeError:
            Config.dimcolor = QColor('#000000')
            Config.dimcolor.setAlpha(0)

        try:
            Config.DateLocale
        except AttributeError:
            Config.DateLocale = ''

        try:
            Config.digital
        except AttributeError:
            Config.digital = 0

        try:
            Config.Language
        except AttributeError:
            try:
                Config.Language = Config.wuLanguage
            except AttributeError:
                Config.Language = "en"

        #
        # Check if Mapbox API key is set, and use mapbox if so
        try:
            if ApiKeys.mbapi[:3].lower() == "pk.":
                Config.usemapbox = 1
        except AttributeError:
            pass
        return Config
