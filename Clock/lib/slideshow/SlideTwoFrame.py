from PySide2 import QtWidgets, QtGui, QtCore, QtNetwork
from PySide2.QtGui import QPixmap, QBrush, QColor
from PySide2.QtCore import Qt

class SlideTwoFrame(QtWidgets.QFrame):
    def __init__(self, parent, config, width, height):
        QtWidgets.QFrame.__init__(self, parent)
        xscale = float(width) / 1440.0
        yscale = float(height) / 900.0
        self.setObjectName("slideTwoFrame")
        self.setGeometry(0, 0, width, height)
        self.setStyleSheet("#slideTwoFrame { background-color: black; border-image: url(" +
                     config.mainclassicbg + ") 0 0 0 0 stretch stretch;}")
        self.clockIqamah = QtWidgets.QLabel(self)
        self.clockIqamah.setObjectName("clockIqamah")
        self.clockIqamah.setGeometry(30 * xscale, 25 * yscale, 370 * xscale, 100 * yscale)
        dcolor = QColor(config.digitalcolor3).darker(0).name()
        lcolor = QColor(config.digitalcolor3).lighter(120).name()
        self.clockIqamah.setStyleSheet(
           "#clockIqamah { background-color: white; font-family:Trebuchet MS;" +
           " font-weight: bold; color: #434242; font-size: " +
           str(int(80 * xscale)) +
           "px; " +
           config.fontattr +
           "}")
        self.clockIqamah.setAlignment(Qt.AlignCenter)

        self.hadistText = QtWidgets.QLabel(self)
        self.hadistText.setObjectName("hadistText")
        self.hadistText.setWordWrap(True) 
        self.hadistText.setStyleSheet("#hadistText { font-family:Trebuchet MS; font-style: italic;font-weight:light italic ;color: " +
                            config.textcolor2 +
                             "; background-color: transparent; font-size: " +
                             str(int(25 * xscale)) + "px; " +
                             config.fontattr +
                             "}")
        self.hadistText.setAlignment(Qt.AlignLeft)
        self.hadistText.setGeometry(400 * xscale, 300 * yscale, 700 * xscale, 1000 * yscale)
        self.hadistText.setText(config.hadist)

        self.insertPictIqamah = QtWidgets.QLabel(self)
        self.insertPictIqamah.setObjectName("insertPictIqamah")
        self.insertPictIqamah.setStyleSheet("#insertPictIqamah {background-color: transparent;}")
        pixmap = QtGui.QPixmap(config.logo)
        height_label = 130
        width_label = 130
        self.insertPictIqamah.resize(width_label, height_label)
        self.insertPictIqamah.setPixmap(pixmap.scaled(self.insertPictIqamah.size(), QtCore.Qt.KeepAspectRatio))
        self.insertPictIqamah.setGeometry(650 * xscale, 30 * yscale, 150 * xscale, 100 * yscale)


    def updateClock(self, timestr):
        self.clockIqamah.setText(timestr.lower())
