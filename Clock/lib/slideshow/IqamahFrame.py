from PySide2 import QtWidgets, QtGui, QtCore, QtNetwork
from PySide2.QtGui import QPixmap, QBrush, QColor
from PySide2.QtCore import Qt

class IqamahFrame(QtWidgets.QFrame):
    def __init__(self, parent, config, width, height):
        QtWidgets.QFrame.__init__(self, parent)
        xscale = float(width) / 1440.0
        yscale = float(height) / 900.0
        self.setObjectName("iqamahFrame")
        self.setGeometry(0, 0, width, height)
        self.setStyleSheet("#iqamahFrame { background-color: transparent; border-image: url(" +
                     config.mainclassicbg+") 0 0 0 0 stretch stretch;}")
        self.iqamahCountdown = QtWidgets.QLabel(self)
        self.iqamahCountdown.setObjectName("iqamahCountdown")
        iqamahCountdownRect = QtCore.QRect(
            width / 2 - height * .4,
            height * .45 - height * .25,
            height * .8,
            height * .8)
        self.iqamahCountdown.setGeometry(iqamahCountdownRect)
        dcolor = QColor(config.digitalcolor5).darker(0).name()
        lcolor = QColor(config.digitalColorCountdownTarhim).lighter(150).name()
        self.iqamahCountdown.setStyleSheet(
            "#iqamahCountdown { background-color: transparent; font-family:Trebuchet MS;" +
            " font-weight: bold; color: " +
            lcolor +
            "; background-color: transparent; font-size: " +
            str(int(config.digitalsizeContdownTarhim * xscale)) +
            "px; " +
            config.fontattr +
            "}")
        self.iqamahCountdown.setAlignment(Qt.AlignCenter)
        glow = QtWidgets.QGraphicsDropShadowEffect()
        glow.setOffset(0)
        glow.setBlurRadius(50)
        glow.setColor(QColor(dcolor))
        self.iqamahCountdown.setGraphicsEffect(glow)

        self.PictIqamah = QtWidgets.QLabel(self)
        self.PictIqamah.setObjectName("PictIqamah")
        self.PictIqamah.setStyleSheet("#PictIqamah {background-color: transparent;}")
        pixmap = QtGui.QPixmap(config.logo)
        height_label = 130
        width_label = 130
        self.PictIqamah.resize(width_label, height_label)
        self.PictIqamah.setPixmap(pixmap.scaled(self.PictIqamah.size(), QtCore.Qt.KeepAspectRatio))
        self.PictIqamah.setGeometry(650 * xscale, 30 * yscale, 150 * xscale, 100 * yscale)

    def setCountdown(self, deltastr):
        self.iqamahCountdown.setText(deltastr)
