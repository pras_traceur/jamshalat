from PySide2 import QtWidgets, QtGui, QtCore, QtNetwork
from PySide2.QtGui import QPixmap, QBrush, QColor
from PySide2.QtCore import Qt

islamicMonths = {
1:'Muharram', 2:'Shafar', 3:'Rabiul Awwal', 4:'Rabiul Akhir', 5:'Jumadil Awwal', 6:'Jumadil Akhir',
7:'Rajab', 8:'Sya\'ban', 9:'Ramadhan', 10:'Syawwal', 11:'Dzulqa\'idah', 12:'Dzulhijjah'
}

class SlideOneFrame(QtWidgets.QFrame):
    def __init__(self, parent, config, width, height):
        QtWidgets.QFrame.__init__(self, parent)
        xscale = float(width) / 1440.0
        yscale = float(height) / 900.0
        self.setObjectName("slideOneFrame")
        self.setGeometry(0, 0, width, height)
        self.setStyleSheet("#slideOneFrame { background-color: black; border-image: url(" +
                     config.mainclassicbg + ") 0 0 0 0 stretch stretch;}")
        self.clockTarhim = QtWidgets.QLabel(self)
        self.clockTarhim.setObjectName("clockTarhim")
        self.clockTarhim.setGeometry(30 * xscale, 25 * yscale, 370 * xscale, 100 * yscale)
        dcolor = QColor(config.digitalcolor3).darker(0).name()
        lcolor = QColor(config.digitalcolor3).lighter(120).name()
        self.clockTarhim.setStyleSheet(
           "#clockTarhim { background-color: white; font-family:Trebuchet MS;" +
           " font-weight: bold; color: #434242; font-size: " +
           str(int(80 * xscale)) +
           "px; " +
           config.fontattr +
           "}")
        self.clockTarhim.setAlignment(Qt.AlignCenter)

        self.todayDayTarhim = QtWidgets.QLabel(self)
        self.todayDayTarhim.setObjectName("todayDayTarhim")
        self.todayDayTarhim.setStyleSheet("#todayDayTarhim { font-family:Trebuchet MS; font-weight:light; color: " +
                            "white " +
                            "; background-color: transparent; font-size: " +
                            str(int(40 * xscale)) +
                            "px; " +
                            config.fontattr +
                            "}")
        self.todayDayTarhim.setAlignment(Qt.AlignLeft)
        self.todayDayTarhim.setGeometry(30 * xscale, 135 * yscale, 500 * xscale, 100)

        self.todayDateTarhim = QtWidgets.QLabel(self)
        self.todayDateTarhim.setObjectName("todayDateTarhim")
        self.todayDateTarhim.setStyleSheet("#todayDateTarhim { font-family:Trebuchet MS; font-weight:light; color: " +
                            config.textcolor2 +
                            "; background-color: transparent; font-size: " +
                            str(int(40 * xscale)) +
                            "px; " +
                            config.fontattr +
                            "}")
        self.todayDateTarhim.setAlignment(Qt.AlignLeft)
        self.todayDateTarhim.setGeometry(30 * xscale, 180 * yscale, 500 * xscale, 100)

        self.hijriDateTarhim = QtWidgets.QLabel(self)
        self.hijriDateTarhim.setObjectName("hijriDateTarhim")
        self.hijriDateTarhim.setStyleSheet("#hijriDateTarhim { font-family:Trebuchet MS; font-weight:light; color: " +
                            config.textcolor2 +
                            "; background-color: transparent; font-size: " +
                            str(int(40 * xscale)) +
                            "px; " +
                            config.fontattr +
                            "}")
        self.hijriDateTarhim.setAlignment(Qt.AlignLeft)
        self.hijriDateTarhim.setGeometry(30 * xscale, 225 * yscale, 500 * xscale, 100)

        self.datex3 = QtWidgets.QLabel(self)
        self.datex3.setObjectName("datex3")
        self.datex3.setStyleSheet("#datex3 { font-family:sans-serif; color: " +
                             config.textcolor +
                             "; background-color: transparent; font-size: " +
                             str(int(50 * xscale)) + "px; " +
                             config.fontattr +
                             "}")
        self.datex3.setAlignment(Qt.AlignHCenter | Qt.AlignTop)
        self.datex3.setGeometry(800 * xscale, 780 * yscale, 640 * xscale, 100)

        self.namaAcara = QtWidgets.QLabel(self)
        self.namaAcara.setObjectName("namaAcara")
        self.namaAcara.setStyleSheet("#namaAcara { font-family:Trebuchet MS; font-style: italic;font-weight:light italic ;color: " +
                            config.textcolor2 +
                             "; background-color: transparent; font-size: " +
                             str(int(30 * xscale)) + "px; " +
                             config.fontattr +
                             "}")
        self.namaAcara.setAlignment(Qt.AlignLeft)
        self.namaAcara.setGeometry(400 * xscale, 300 * yscale, 700 * xscale, 1000 * yscale)

        self.temaAcara = QtWidgets.QLabel(self)
        self.temaAcara.setObjectName("temaAcara")
        self.temaAcara.setStyleSheet("#temaAcara { font-family:Trebuchet MS; font-style: italic;font-weight:light italic ;color: " +
                            config.textcolor2 +
                             "; background-color: transparent; font-size: " +
                             str(int(30 * xscale)) + "px; " +
                             config.fontattr +
                             "}")
        self.temaAcara.setAlignment(Qt.AlignLeft)
        self.temaAcara.setGeometry(400 * xscale, 360 * yscale, 700 * xscale, 1000 * yscale)

        self.tempatAcara = QtWidgets.QLabel(self)
        self.tempatAcara.setObjectName("tempatAcara")
        self.tempatAcara.setStyleSheet("#tempatAcara { font-family:Trebuchet MS; font-style: italic;font-weight:light italic ;color: " +
                            config.textcolor2 +
                             "; background-color: transparent; font-size: " +
                             str(int(30 * xscale)) + "px; " +
                             config.fontattr +
                             "}")
        self.tempatAcara.setAlignment(Qt.AlignLeft)
        self.tempatAcara.setGeometry(400 * xscale, 420 * yscale, 700 * xscale, 1000 * yscale)

        self.tanggalAcara = QtWidgets.QLabel(self)
        self.tanggalAcara.setObjectName("tanggalAcara")
        self.tanggalAcara.setStyleSheet("#tanggalAcara { font-family:Trebuchet MS; font-style: italic;font-weight:light italic ;color: " +
                            config.textcolor2 +
                             "; background-color: transparent; font-size: " +
                             str(int(30 * xscale)) + "px; " +
                             config.fontattr +
                             "}")
        self.tanggalAcara.setAlignment(Qt.AlignLeft)
        self.tanggalAcara.setGeometry(400 * xscale, 480 * yscale, 700 * xscale, 1000 * yscale)

        self.waktuAcara = QtWidgets.QLabel(self)
        self.waktuAcara.setObjectName("waktuAcara")
        self.waktuAcara.setStyleSheet("#waktuAcara { font-family:Trebuchet MS; font-style: italic;font-weight:light italic ;color: " +
                            config.textcolor2 +
                             "; background-color: transparent; font-size: " +
                             str(int(30 * xscale)) + "px; " +
                             config.fontattr +
                             "}")
        self.waktuAcara.setAlignment(Qt.AlignLeft)
        self.waktuAcara.setGeometry(400 * xscale, 540 * yscale, 700 * xscale, 1000 * yscale)

        self.keteranganAcara = QtWidgets.QLabel(self)
        self.keteranganAcara.setObjectName("keteranganAcara")
        self.keteranganAcara.setStyleSheet("#keteranganAcara { font-family:Trebuchet MS; font-style: italic;font-weight:light italic ;color: " +
                            config.textcolor2 +
                             "; background-color: transparent; font-size: " +
                             str(int(30 * xscale)) + "px; " +
                             config.fontattr +
                             "}")
        self.keteranganAcara.setAlignment(Qt.AlignLeft)
        self.keteranganAcara.setGeometry(400 * xscale, 600 * yscale, 700 * xscale, 1000 * yscale)

        self.insertPictTarhim = QtWidgets.QLabel(self)
        self.insertPictTarhim.setObjectName("insertPictTarhim")
        self.insertPictTarhim.setStyleSheet("#insertPictTarhim {background-color: transparent;}")
        pixmap = QtGui.QPixmap(config.logo)
        height_label = 130
        width_label = 130
        self.insertPictTarhim.resize(width_label, height_label)
        self.insertPictTarhim.setPixmap(pixmap.scaled(self.insertPictTarhim.size(), QtCore.Qt.KeepAspectRatio))
        self.insertPictTarhim.setGeometry(650 * xscale, 10 * yscale, 150 * xscale, 100 * yscale)



    def updateClock(self, timestr):
        self.clockTarhim.setText(timestr.lower())

    def updateDate(self, day, ds, islamicDate):
        self.todayDayTarhim.setText(day)
        self.todayDateTarhim.setText(ds)
        self.hijriDateTarhim.setText("{0} {1} {2}".format(islamicDate[2], islamicMonths[islamicDate[1]], islamicDate[0]))

    def updateKajianDisplay(self,kajian):
        self.namaAcara.setText("Acara : "+kajian['fields']['nama_acara'])
        self.temaAcara.setText("Tema : "+kajian['fields']['tema'])
        self.tempatAcara.setText("Tempat : "+kajian['fields']['tempat'])
        self.tanggalAcara.setText("Tanggal : "+kajian['fields']['tanggal_kadaluarsa'])
        self.waktuAcara.setText("Waktu : "+kajian['fields']['waktu_kadaluarsa'])
        self.keteranganAcara.setText("Keterangan : "+kajian['fields']['keterangan'])

