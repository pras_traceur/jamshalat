from PySide2 import QtWidgets, QtGui, QtCore, QtNetwork
from PySide2.QtGui import QPixmap, QBrush, QColor
from PySide2.QtCore import Qt
# encapsulate clock, Reza
class ClockFace():

    def __init__(self, name, frame, rect, timeDisplay, config, xscale):

        self.name = name
        self.frame = frame
        self.rect = rect

        self.timeDisplay = timeDisplay
        self.config = config
        self.xscale = xscale

    def highlight(self):
        self.frame.setStyleSheet("#main"+self.name+" { font-family:Trebuchet MS; font-weight: bold;color: " +
                        "white" +
                         "; background-color: red; font-size: " +
                         str(int(40 * self.xscale)) + "px; " +
                         self.config.fontattr +
                         "}")
    def unhighlight(self):
        self.frame.setStyleSheet("#main"+self.name+" { font-family:Trebuchet MS; font-weight: bold;color: " +
                        "white" +
                        "; background-color : transparent; font-size: " +
                         str(int(40 * self.xscale)) + "px; " +
                         self.config.fontattr +
                         "}")

    def draw(self, time):
        self.timeDisplay.setText(time)

    def makeClock(name, frame,
            x, y,
            w, h,
            url,

            mainX, mainY,
            mainWidth, mainHeight,
            mainLabel,
            config, xscale
            ):
        timeDisplay = None
        face = None
        rect = None
        hourHand = None
        minHand = None
        secHand = None
        hourPixmap = None
        hourpixmap26 = None
        minPixmap = None
        minpixmap26 = None
        secPixmap = None
        secpixmap26 = None

        main = QtWidgets.QLabel(frame)
        main.setObjectName("main"+name)
        main.setStyleSheet("#main"+name+" { font-family:Trebuchet MS; color: " +
                            config.textcolor2 +
                             "; font-size: " +
                             str(int(40 * xscale)) + "px; " +
                             config.fontattr +
                             "}")
        main.setAlignment(Qt.AlignLeft)
        main.setGeometry(mainX, mainY, mainWidth, mainHeight)
        main.setText(mainLabel)
        # main.setGraphicsEffect(glow)
      

        timeDisplay = QtWidgets.QLabel(frame)
        timeDisplay.setObjectName("time"+name)
        timeDisplay.setStyleSheet("#time"+name+" { font-family:Trebuchet MS; color: " +
                            config.textcolor2 +
                             "; background-color: transparent; font-size: " +
                         str(int(40 * xscale)) + "px; font-weight:bold; " + 
                             config.fontattr +
                             "}")
        timeDisplay.setAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
        timeDisplay.setGeometry(x-5, y, w+10, h)

        main.stackUnder(timeDisplay)

        # return ClockFace(name, face, rect, minPixmap, minHand, hourPixmap, hourHand, secPixmap, secHand, timeDisplay, main)
        return ClockFace(name, main, rect, timeDisplay, config, xscale)


class MainSlideFrame(QtWidgets.QFrame):
    def __init__(self, parent, config, width, height):
        QtWidgets.QFrame.__init__(self, parent)
        xscale = float(width) / 1440.0
        yscale = float(height) / 900.0
        self.config = config
        self.xscale = xscale
        self.yscale = yscale
        self.islamicMonths = {
        1:'Muharram', 2:'Shafar', 3:'Rabiul Awwal', 4:'Rabiul Akhir', 5:'Jumadil Awwal', 6:'Jumadil Akhir',
        7:'Rajab', 8:'Sya\'ban', 9:'Ramadhan', 10:'Syawwal', 11:'Dzulqa\'idah', 12:'Dzulhijjah'
        }


        self.setObjectName("mainSlideFrame")
        self.setGeometry(0, 0, width, height)
        self.setStyleSheet("#mainSlideFrame { background-color: black; border-image: url(" +
                     config.mainclassicbg + ") 0 0 0 0 stretch stretch;}")
        self.clockMain = QtWidgets.QLabel(self)
        self.clockMain.setObjectName("clockMain")
        self.clockMain.setGeometry(30 * xscale, 25 * yscale, 370 * xscale, 100 * yscale)
        dcolor = QColor(config.digitalcolor3).darker(0).name()
        lcolor = QColor(config.digitalcolor3).lighter(120).name()
        self.clockMain.setStyleSheet(
           "#clockMain { background-color: white; font-family:Trebuchet MS;" +
           " font-weight: bold; color: #434242; font-size: " +
           str(int(80 * xscale)) +
           "px; " +
           config.fontattr +
           "}")
        self.clockMain.setAlignment(Qt.AlignCenter)


        #Set Label waktu adzan dan masjid, hendra
        self.namaMasjid = QtWidgets.QLabel(self)
        self.namaMasjid.setObjectName("namaMasjid")
        self.namaMasjid.setWordWrap(True) 
        self.namaMasjid.setStyleSheet("#namaMasjid { font-family:Trebuchet MS; font-style: italic;font-weight:light italic ;color: " +
                            config.textcolor2 +
                             "; background-color: transparent; font-size: " +
                             str(int(40 * xscale)) + "px; " +
                             config.fontattr +
                             "}")
        self.namaMasjid.setAlignment(Qt.AlignCenter)
        self.namaMasjid.setGeometry(50 * xscale, 70 * yscale, 400 * xscale, 1000 * yscale)
        self.namaMasjid.setText(config.mainInfo)

        self.insertPict = QtWidgets.QLabel(self)
        self.insertPict.setObjectName("insertPict")
        self.insertPict.setStyleSheet("#insertPict {background-color: transparent;}")
        pixmap = QtGui.QPixmap(config.logo)
        height_label = 130
        width_label = 130
        self.insertPict.resize(width_label, height_label)
        self.insertPict.setPixmap(pixmap.scaled(self.insertPict.size(), QtCore.Qt.KeepAspectRatio))
        self.insertPict.setGeometry(650 * xscale, 10 * yscale, 150 * xscale, 100 * yscale)

        self.labelWaktu = QtWidgets.QLabel(self)
        self.labelWaktu.setObjectName("labelWaktu")
        self.labelWaktu.setStyleSheet("#labelWaktu { font-family:Trebuchet MS; font-weight:bold; color: " +
                            "white " +
                            "; background-color: transparent; font-size: " +
                            str(int(40 * xscale)) +
                            "px; " +
                            config.fontattr +
                            "}")
        self.labelWaktu.setAlignment(Qt.AlignLeft)
        self.labelWaktu.setGeometry(1100 * xscale, 170 * yscale, 500 * xscale, 100)
        self.labelWaktu.setText("Waktu Shalat")

        # Set Tanggal frame 1, hendra
        # dcolor = QColor(config.digitalcolor).darker(0).name()
        # lcolor = QColor(config.digitalcolor).lighter(120).name()
        # glow = QtWidgets.QGraphicsDropShadowEffect()
        # glow.setOffset(0)
        # glow.setBlurRadius(50)
        # glow.setColor(QColor(dcolor))
        self.todayDay = QtWidgets.QLabel(self)
        self.todayDay.setObjectName("todayDay")
        self.todayDay.setStyleSheet("#todayDay { font-family:Trebuchet MS; font-weight:light; color: " +
                            "white " +
                            "; background-color: transparent; font-size: " +
                            str(int(40 * xscale)) +
                            "px; " +
                            config.fontattr +
                            "}")
        self.todayDay.setAlignment(Qt.AlignLeft)
        self.todayDay.setGeometry(30 * xscale, 135 * yscale, 500 * xscale, 100)
        #todayDay.setGraphicsEffect(glow)

        self.todayDate = QtWidgets.QLabel(self)
        self.todayDate.setObjectName("todayDate")
        self.todayDate.setStyleSheet("#todayDate { font-family:Trebuchet MS; font-weight:light; color: " +
                            config.textcolor2 +
                            "; background-color: transparent; font-size: " +
                            str(int(40 * xscale)) +
                            "px; " +
                            config.fontattr +
                            "}")
        self.todayDate.setAlignment(Qt.AlignLeft)
        self.todayDate.setGeometry(30 * xscale, 180 * yscale, 500 * xscale, 100)

        self.hijriDate = QtWidgets.QLabel(self)
        self.hijriDate.setObjectName("hijriDate")
        self.hijriDate.setStyleSheet("#hijriDate { font-family:Trebuchet MS; font-weight:light; color: " +
                            config.textcolor2 +
                            "; background-color: transparent; font-size: " +
                            str(int(40 * xscale)) +
                            "px; " +
                            config.fontattr +
                            "}")
        self.hijriDate.setAlignment(Qt.AlignLeft)
        self.hijriDate.setGeometry(30 * xscale, 225 * yscale, 500 * xscale, 100)


        y1 = (height * 1 - height * .275)
        mainY = height - height * .2 - 100
        mainHeight = 150
        clockWidth = (height * .15)
        clockHeight = (height * .13)
        clockPadding = 35
        prayTimeWidth = width / 5 - 2 * clockPadding

        # SHUBUH
        self.shubuhClock = ClockFace.makeClock("shubuh",self,
            1150 * xscale,
            212 * yscale,
            clockWidth,
            clockHeight,
            config.clockface,

            850 * xscale,
            240 * yscale,
            prayTimeWidth + 250,
            mainHeight - 100,
            "Shubuh",
            self.config,
            self.xscale
            )
            
        #SYURUQ
        self.syuruqClock = ClockFace.makeClock("syuruq",self,
            1150 * xscale,
            275 * yscale,
            clockWidth,
            clockHeight,
            config.clockface,

            850 * xscale,
            300 * yscale,
            prayTimeWidth + 250,
            mainHeight - 100,
            "Fajr",
            self.config,
            self.xscale
            )

        # DHUHUR
        self.dhuhurClock = ClockFace.makeClock("dhuhur",self,
            1150 * xscale,
            330 * yscale,
            clockWidth,
            clockHeight,
            config.clockface,

            850 * xscale,
            360 * yscale,
            prayTimeWidth + 250,
            mainHeight - 100,
            "Dzuhur",
            self.config,
            self.xscale
            )

        # ASHR
        self.asharClock = ClockFace.makeClock("ashar",self,
            1150 * xscale,
            395 * yscale,
            clockWidth,
            clockHeight,
            config.clockface,

            850 * xscale,
            425 * yscale,
            prayTimeWidth + 250,
            mainHeight - 100,
            "Ashr",
            self.config,
            self.xscale
            )


        # MAGHRIB
        self.maghribClock = ClockFace.makeClock("maghrib",self,
            1150 * xscale,
            455 * yscale,
            clockWidth,
            clockHeight,
            config.clockface,

            850 * xscale,
            485 * yscale,
            prayTimeWidth + 250,
            mainHeight - 100,
            "Maghrib",
            self.config,
            self.xscale
            )

        # ISHA
        self.ishaClock = ClockFace.makeClock("isha",self,
            1150 * xscale,
            515 * yscale,
            clockWidth,
            clockHeight,
            config.clockface,

            850 * xscale,
            545 * yscale,
            prayTimeWidth + 250,
            mainHeight - 100,
            "Isya",
            self.config,
            self.xscale
            )
    def time_active(self,idx):
        
        if idx == 1:
            self.shubuhClock.highlight()
        else:
            self.shubuhClock.unhighlight()

        if idx == 2:
            self.syuruqClock.highlight()
        else:
            self.syuruqClock.unhighlight()

        if idx == 3:
            self.dhuhurClock.highlight()
        else:
            self.dhuhurClock.unhighlight()

        if idx == 4:
            self.asharClock.highlight()
        else:
            self.asharClock.unhighlight()

        if idx == 5:
            self.maghribClock.highlight()
        else:
            self.maghribClock.unhighlight()

        if idx == 0:
            self.ishaClock.highlight()
        else:
            self.ishaClock.unhighlight()

    def updateClock(self, timestr):
        self.clockMain.setText(timestr.lower())

    def updateDate(self, day, ds, islamicDate):
        self.todayDay.setText(day)
        self.todayDate.setText(ds)
        self.hijriDate.setText("{0} {1} {2}".format(islamicDate[2], self.islamicMonths[islamicDate[1]], islamicDate[0]))


    def updateSholatDisplay(self, data_adzan):
        self.shubuhClock.draw(data_adzan[0])
        self.syuruqClock.draw(data_adzan[1])
        self.dhuhurClock.draw(data_adzan[2])
        self.asharClock.draw(data_adzan[3])
        self.maghribClock.draw(data_adzan[4])
        self.ishaClock.draw(data_adzan[5])


