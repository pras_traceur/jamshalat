from PySide2 import QtWidgets, QtGui, QtCore
from PySide2.QtCore import Qt

class AnalogClock():

    def __init__(self, name, frame, rect, minutePixmap, minuteHand, hourPixmap, hourHand, secondPixmap, secondHand):

        self.name = name
        self.frame = frame
        self.rect = rect
        self.minutePixmap = minutePixmap
        self.minuteHand = minuteHand
        self.hourPixmap = hourPixmap
        self.hourHand = hourHand
        self.secondPixmap = secondPixmap
        self.secondHand = secondHand
        self.lastmin = None

    def drawHand(self, angle, rect, pixmap, hand):
        ts = pixmap.size()
        pixmap2 = pixmap.transformed(
            QtGui.QMatrix().scale(
                float(rect.width()) / ts.height(),
                float(rect.height()) / ts.height()
            ).rotate(angle),
            Qt.SmoothTransformation
        )
        hand.setPixmap(pixmap2)
        ts = pixmap2.size()
        hand.setGeometry(
            rect.center().x() - ts.width() / 2,
            rect.center().y() - ts.height() / 2,
            ts.width(),
            ts.height()
        )

    def draw(self,now):
        angle = now.second * 6
        self.drawHand(angle, self.rect, self.secondPixmap, self.secondHand)

        if now.minute != self.lastmin:
            self.lastmin = now.minute

            angle = now.minute * 6
            self.drawHand(angle, self.rect, self.minutePixmap, self.minuteHand)

            angle = ((now.hour % 12) + now.minute / 60.0) * 30.0
            self.drawHand(angle, self.rect, self.hourPixmap, self.hourHand)


    def makeClock(name, frame,
            x, y,
            w, h,
            clockfaceFilename,
            hourHandFilename,
            minHandFilename,
            secHandFilename,
            ):
        face = None
        rect = None
        hourHand = None
        minHand = None
        secHand = None
        hourPixmap = None
        minPixmap = None
        secPixmap = None

        face = QtWidgets.QFrame(frame)
        face.setObjectName("clock"+name)
        rect = QtCore.QRect(
            x, y,
            w, h)
        face.setGeometry(rect)
        face.setStyleSheet(
            "#clock"+name+" { background-color: transparent; border-image: url(" +
            clockfaceFilename +
            ") 0 0 0 0 stretch stretch;}")

        hourHand = QtWidgets.QLabel(frame)
        hourHand.setObjectName("hour"+name)
        hourHand.setStyleSheet("#hour"+name+" { background-color: transparent; }")

        minHand = QtWidgets.QLabel(frame)
        minHand.setObjectName("min"+name)
        minHand.setStyleSheet("#min"+name+" { background-color: transparent; }")

        secHand = QtWidgets.QLabel(frame)
        secHand.setObjectName("sec"+name)
        secHand.setStyleSheet("#sec"+name+" { background-color: transparent; }")

        hourPixmap = QtGui.QPixmap(hourHandFilename)
        minPixmap = QtGui.QPixmap(minHandFilename)
        secPixmap = QtGui.QPixmap(secHandFilename)

        return AnalogClock(name, face, rect, minPixmap, minHand, hourPixmap, hourHand, secPixmap, secHand)
