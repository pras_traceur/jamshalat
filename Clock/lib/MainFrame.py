from PySide2 import QtWidgets, QtGui, QtCore, QtNetwork
from PySide2.QtCore import Qt
from PySide2.QtGui import QColor

import datetime
from datetime import date
from convertdate import islamic


# encapsulate clock, Reza
class ClockFace():

    def __init__(self, name, frame, rect, timeDisplay, parentFrame, config, xscale):

        self.name = name
        self.frame = frame
        self.rect = rect
        
        self.timeDisplay = timeDisplay
        self.parentFrame = parentFrame
        self.config = config
        self.xscale = xscale

    def highlight(self):
        self.parentFrame.setStyleSheet("#main"+self.name+" { font-family:courier; border:3px solid red;border-radius: 25px; color: " +
                        "white" +
                         "; background-color: red; font-size: " +
                         str(int(25 * self.xscale)) + "px; " +
                         self.config.fontattr +
                         "}")
    def unhighlight(self):
        self.parentFrame.setStyleSheet("#main"+self.name+"{background-color: rgba(0, 0, 0, 200); border-radius: 25px; color: red;}")
        self.parentFrame.setStyleSheet("#main"+self.name+" { font-family:courier; border:3px solid red;border-radius: 25px; color: " +
                        self.config.textcolor2 +
                         "; background-color: rgba(0, 0, 0, 200); font-size: " +
                         str(int(25 * self.xscale)) + "px; " +
                         self.config.fontattr +
                         "}")


    def draw(self,time):
        self.timeDisplay.setText(time)

    def makeClock(name, frame,
            mainX, mainY,
            mainWidth, mainHeight,
            mainLabel, config, xscale
            ):
        timeDisplay = None
        face = None
        rect = None

        main = QtWidgets.QLabel(frame)
        main.setObjectName("main"+name)
        main.setStyleSheet("#main"+name+" { font-family:courier; border:3px solid red;border-radius: 25px; color: " +
                            config.textcolor2 +
                             "; background-color: rgba(0, 0, 0, 200); font-size: " +
                             str(int(25 * xscale)) + "px; " +
                             config.fontattr +
                             "}")
        main.setAlignment(Qt.AlignHCenter | Qt.AlignTop)
        main.setGeometry(mainX, mainY, mainWidth, mainHeight)
        main.setText(mainLabel)

        timeDisplay = QtWidgets.QLabel(frame)
        timeDisplay.setObjectName("time"+name)
        timeDisplay.setStyleSheet("#time"+name+" { font-family:Arial,sans-serif; color: " +
                            config.textcolor2 +
                             "; background-color: transparent; font-size: " +
                         str(int(50 * xscale)) + "px; font-weight:bold; " + 
                             config.fontattr +
                             "}")
        timeDisplay.setAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
        timeDisplay.setGeometry(mainX, mainY+20, mainWidth, mainHeight-20)

        main.stackUnder(timeDisplay)

        return ClockFace(name, face, rect, timeDisplay, main, config, xscale)

class MainFrame(QtWidgets.QFrame):
    def __init__(self, parent, config, width, height):

        QtWidgets.QFrame.__init__(self, parent)

        self.lasttimestr = ''
        self.fiveSec = 5
        self.news = ["Rukun Islam terdiri daripada lima perkara, yaitu:","Syahadat: menyatakan kalimat tiada Tuhan selain Allah, dan Muhammad itu utusan Allah.","Shalat: ibadah sembahyang lima waktu sehari","Saum: berpuasa dan mengendalikan diri selama bulan suci Ramadan","Zakat: memberikan 2,5% dari uang simpanan kepada orang miskin atau yang membutuhkan","Haji: pergi beribadah ke Mekkah, setidaknya sekali seumur hidup bagi mereka yang mampu"]
        self.newsindex = 0
        self.config = config
        self.xscale = float(width) / 1440.0
        self.yscale = float(height) / 900.0

        self.islamicMonths = {
        1:'Muharram', 2:'Shafar', 3:'Rabiul Awwal', 4:'Rabiul Akhir', 5:'Jumadil Awwal', 6:'Jumadil Akhir',
        7:'Rajab', 8:'Sya\'ban', 9:'Ramadhan', 10:'Syawwal', 11:'Dzulqa\'idah', 12:'Dzulhijjah'
        }



        self.setObjectName("mainFrame")
        # self.setGeometry(rect)
        self.setGeometry(0, 0, width, height)
        self.setStyleSheet("#mainFrame { background-color: black; border-image: url(" +
                     config.background9 + ") 0 0 0 0 stretch stretch;}")

        self.lastmin = -1


        print("make mainClock")

        mainClockBgColor = "rgba(255,0,0,200)"
        mainClockWidth = 80 * self.xscale
        mainClockGapWidth = 10 * self.xscale
        mainClockHeight = 100 * self.yscale
        mainClockYPos  = 68 * self.yscale
        mainClockLeft = 1091 * self.xscale

        mainClockStylesheet = "".join([
            "{ font-family:comic-sans;" +
            " font-weight: bold; color: white; background-color: "+mainClockBgColor+"; font-size: ",
            str(int(config.mainframeClockDigitSize * self.xscale)),
            "px; ",
            config.fontattr,
            "}"
            ])
        mainClockStylesheet = "".join([
            "{ font-family:comic-sans;",
            " font-weight: bold; color: white; border-radius: 5px; border:2px solid red; background-color: "+mainClockBgColor+"; font-size: ",
            str(int(config.mainframeClockDigitSize * self.xscale)),
            "px; ",
            config.fontattr,
            "}"
            ])

        self.mainClockHour = QtWidgets.QLabel(self)
        self.mainClockHour.setObjectName("mainClockHour")
        self.mainClockHour.setStyleSheet("#mainClockHour "+mainClockStylesheet)
        self.mainClockHour.setAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
        self.mainClockHour.setGeometry(mainClockLeft, mainClockYPos, mainClockWidth, mainClockHeight)

        self.mainClockMinute = QtWidgets.QLabel(self)
        self.mainClockMinute.setObjectName("mainClockMinute")
        self.mainClockMinute.setStyleSheet("#mainClockMinute "+mainClockStylesheet)
        self.mainClockMinute.setAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
        self.mainClockMinute.setGeometry(
            mainClockLeft + mainClockWidth + mainClockGapWidth,
            mainClockYPos, mainClockWidth, mainClockHeight)

        self.mainClockSecond = QtWidgets.QLabel(self)
        self.mainClockSecond.setObjectName("mainClockSecond")
        self.mainClockSecond.setStyleSheet("#mainClockSecond  "+mainClockStylesheet)
        self.mainClockSecond.setAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
        self.mainClockSecond.setGeometry(
            mainClockLeft + mainClockWidth + mainClockGapWidth + mainClockWidth + mainClockGapWidth,
            mainClockYPos, mainClockWidth, mainClockHeight)

        print("made mainClock")

        # Set Tanggal frame 1, hendra
        dcolor = QColor(config.digitalcolor).darker(0).name()
        self.todayDay = QtWidgets.QLabel(self)
        self.todayDay.setObjectName("todayDay")
        self.todayDay.setStyleSheet("#todayDay { font-family:Arial; font-weight:bold; color: " +
                            config.textcolor2 +
                            "; background-color: transparent; font-size: " +
                            str(int(44 * self.xscale)) +
                            "px; " +
                            config.fontattr +
                            "}")
        self.todayDay.setAlignment(Qt.AlignRight)
        self.todayDay.setGeometry(850 * self.xscale, 165 * self.yscale, 500 * self.xscale, 100)

        self.todayDate = QtWidgets.QLabel(self)
        self.todayDate.setObjectName("todayDate")
        self.todayDate.setStyleSheet("#todayDate { font-family:Arial; font-weight:bold; color: " +
                            "red " +
                            "; background-color: transparent; font-size: " +
                            str(int(22 * self.xscale)) +
                            "px; " +
                            config.fontattr +
                            "}")
        self.todayDate.setAlignment(Qt.AlignRight)
        self.todayDate.setGeometry(850 * self.xscale, 220 * self.yscale, 500 * self.xscale, 100)

        self.hijriDate = QtWidgets.QLabel(self)
        self.hijriDate.setObjectName("hijriDate")
        self.hijriDate.setStyleSheet("#hijriDate { font-family:Arial; font-weight:bold; color: " +
                            config.textcolor2 +
                            "; background-color: transparent; font-size: " +
                            str(int(22 * self.xscale)) +
                            "px; " +
                            config.fontattr +
                            "}")
        self.hijriDate.setAlignment(Qt.AlignRight)
        self.hijriDate.setGeometry(850 * self.xscale, 250 * self.yscale, 500 * self.xscale, 100)

        mainY = height - height * .260 
        mainHeight = 150
        clockPadding = 20

        clocksWidth = width * .75 

        print("make 5 clocks")
        # SHUBUH
        prayTimeWidth = clocksWidth / 5 - 2 * clockPadding
        self.shubuhClock = ClockFace.makeClock("shubuh",self,
            0 * ((clocksWidth / 6) + clockPadding) + 130,
            mainY,
            prayTimeWidth,
            mainHeight,
            "SHUBUH",
            self.config,
            self.xscale
            )

        #SYURUQ
        self.syuruqClock = ClockFace.makeClock("syuruq",self,
            1 * ((clocksWidth / 6) + clockPadding) + 130,
            mainY,
            prayTimeWidth,
            mainHeight,
            "SYURUQ",
            self.config,
            self.xscale
            )

        # DHUHUR
        self.dhuhurClock = ClockFace.makeClock("dhuhur",self,
            2 * ((clocksWidth / 6) + clockPadding) + 130,
            mainY,
            prayTimeWidth,
            mainHeight,
            "DZUHUR",
            self.config,
            self.xscale
            )

        # ASHR
        self.asharClock = ClockFace.makeClock("ashar",self,
            3 * ((clocksWidth / 6) + clockPadding) + 130,
            mainY,
            prayTimeWidth,
            mainHeight,
            "ASHR",
            self.config,
            self.xscale
            )


        # MAGHRIB
        self.maghribClock = ClockFace.makeClock("maghrib",self,
            4 * ((clocksWidth / 6) + clockPadding) + 130,
            mainY,
            prayTimeWidth,
            mainHeight,
            "MAGHRIB",
            self.config,
            self.xscale
            )

        # ISHA
        self.ishaClock = ClockFace.makeClock("isha",self,
            5 * ((clocksWidth / 6) + clockPadding) + 130,
            mainY,
            prayTimeWidth,
            mainHeight,
            "ISYA",
            self.config,
            self.xscale
            )
        print("made 5 clocks")

        #Set Label waktu adzan dan masjid, hendra
        print("make identitas masjid dan logo SL")
        self.namaMasjid = QtWidgets.QLabel(self)
        self.namaMasjid.setObjectName("namaMasjid")
        self.namaMasjid.setStyleSheet("#namaMasjid { font-family:Arial,sans-serif; font-weight: bold; color: " +
                            config.textcolor2 +
                             "; background-color: transparent; font-size: " +
                             str(int(56 * self.xscale)) + "px; " +
                             config.fontattr +
                             "}")
        self.namaMasjid.setAlignment(Qt.AlignLeft | Qt.AlignTop)
        self.namaMasjid.setGeometry(1 * self.xscale+20, 70 * self.yscale, 1000 * self.xscale, 500 * self.yscale)
        self.namaMasjid.setText(config.name)

        self.alamatMasjid = QtWidgets.QLabel(self)
        self.alamatMasjid.setObjectName("alamatMasjid")
        self.alamatMasjid.setStyleSheet("#alamatMasjid { font-family:Arial,sans-serif; color: " +
                            config.textcolor2 +
                             "; background-color: transparent; font-size: " +
                             str(int(24 * self.xscale)) + "px; " +
                             config.fontattr +
                             "}")
        self.alamatMasjid.setAlignment(Qt.AlignLeft | Qt.AlignTop | Qt.AlignTop)
        self.alamatMasjid.setGeometry(1 * self.xscale + 22, 130 * self.yscale, 1000 * self.xscale, 500 * self.yscale)
        self.alamatMasjid.setText(config.address)

        self.telpMasjid = QtWidgets.QLabel(self)
        self.telpMasjid.setObjectName("telpMasjid")
        self.telpMasjid.setStyleSheet("#telpMasjid { font-family:Arial,sans-serif; color: " +
                            config.textcolor2 +
                             "; background-color: transparent; font-size: " +
                             str(int(24 * self.xscale)) + "px; " +
                             config.fontattr +
                             "}")
        self.telpMasjid.setAlignment(Qt.AlignLeft | Qt.AlignTop | Qt.AlignTop)
        self.telpMasjid.setGeometry(1 * self.xscale + 22, 160 * self.yscale, 1000 * self.xscale, 500 * self.yscale)
        self.telpMasjid.setText(config.phone)

        self.webInfo = QtWidgets.QLabel(self)
        self.webInfo.setObjectName("webInfo")
        self.webInfo.setStyleSheet("#webInfo { font-family:Arial,sans-serif; color: " +
                            config.textcolor2 +
                             "; background-color: transparent; font-size: " +
                             str(int(20 * self.xscale)) + "px; " +
                             config.fontattr +
                             "}")
        self.webInfo.setAlignment(Qt.AlignLeft | Qt.AlignTop | Qt.AlignTop)
        self.webInfo.setGeometry(1 * self.xscale + 50, 877 * self.yscale, 1000 * self.xscale, 500 * self.yscale)
        self.webInfo.setText(config.website)

        self.insertPict = QtWidgets.QLabel(self)
        self.insertPict.setObjectName("insertPict")
        self.insertPict.setStyleSheet("#insertPict {background-color: transparent;}")
        pixmap = QtGui.QPixmap(config.logo)
        height_label = 100
        width_label = 120
        self.insertPict.resize(width_label, height_label)
        self.insertPict.setPixmap(pixmap.scaled(self.insertPict.size(), QtCore.Qt.KeepAspectRatio))
        self.insertPict.setGeometry(1 * self.xscale + 50, 780 * self.yscale, 150 * self.xscale, 100 * self.yscale)

        self.namaAcara = QtWidgets.QLabel(self)
        self.namaAcara.setObjectName("namaAcara")
        self.namaAcara.setWordWrap(True)
        self.namaAcara.setStyleSheet("#namaAcara { font-family:Arial,sans-serif; color: " +
                            self.config.textcolor2 +
                             "; background-color: transparent; font-size: " +
                             str(int(20 * self.xscale)) + "px; " +
                             self.config.fontattr +
                             "}")
        self.namaAcara.setAlignment(Qt.AlignLeft | Qt.AlignTop)
        self.namaAcara.setGeometry(200 * self.xscale + 50, 840 * self.yscale, 1000 * self.xscale, 500 * self.yscale)


    def time_active(self,idx):
        if idx == 1:
            self.shubuhClock.highlight()
        else:
            self.shubuhClock.unhighlight()

        if idx == 2:
            self.syuruqClock.highlight()
        else:
            self.syuruqClock.unhighlight()

        if idx == 3:
            self.dhuhurClock.highlight()
        else:
            self.dhuhurClock.unhighlight()

        if idx == 4:
            self.asharClock.highlight()
        else:
            self.asharClock.unhighlight()

        if idx == 5:
            self.maghribClock.highlight()
        else:
            self.maghribClock.unhighlight()

        if idx == 0:
            self.ishaClock.highlight()
        else:
            self.ishaClock.unhighlight()


    def updateSholatDisplay(self, data_adzan):
        self.shubuhClock.draw(data_adzan[0])
        self.syuruqClock.draw(data_adzan[1])
        self.dhuhurClock.draw(data_adzan[2])
        self.asharClock.draw(data_adzan[3])
        self.maghribClock.draw(data_adzan[4])
        self.ishaClock.draw(data_adzan[5])

    def updateDate(self, now):

        # date
        sup = 'th'
        if (now.day == 1 or now.day == 21 or now.day == 31):
            sup = 'st'
        if (now.day == 2 or now.day == 22):
            sup = 'nd'
        if (now.day == 3 or now.day == 23):
            sup = 'rd'
        if self.config.DateLocale != "":
            sup = ""
        day = "{0:%A}".format(now)

        ds = "{0.day} {0:%B} {0.year}".format(now)

        islamicDate = islamic.from_gregorian(now.year, now.month, now.day)
        print("ISLAMIC DATE")
        print(islamicDate)
        self.todayDay.setText(self.dayIndonesia(day))
        self.todayDate.setText(ds)
        self.hijriDate.setText("{0} {1} {2}".format(islamicDate[2], self.islamicMonths[islamicDate[1]], islamicDate[0]))

    def dayIndonesia(self,dayname):
        days={
            "Sunday":"Ahad",
            "Monday":"Senin",
            "Tuesday":"Selasa",
            "Wednesday":"Rabu",
            "Thursday":"Kamis",
            "Friday":"Jumat",
            "Saturday":"Sabtu"
        }
        return days.get(dayname)

    def updateKajianDisplay(self, kajian):
        self.namaAcara.setText("Acara : "+kajian['fields']['nama_acara']+", Tema : "+kajian['fields']['tema']+", Tempat : "+kajian['fields']['tempat']+", Tanggal : "+kajian['fields']['tanggal_kadaluarsa']+", Waktu : "+kajian['fields']['waktu_kadaluarsa']+", Keterangan : "+kajian['fields']['keterangan'])

    def tick(self):

        now = datetime.datetime.now()
        timestr = self.config.digitalformat.format(now)
        if self.config.digitalformat.find("%I") > -1:
            if timestr[0] == '0':
                timestr = timestr[1:99]
        if self.lasttimestr != timestr:
            self.setHourText(timestr)
            self.lasttimestr = timestr

    def setHourText(self,timestr):
        self.mainClockHour.setText(timestr.split(":")[0])
        self.mainClockMinute.setText(timestr.split(":")[1])
        self.mainClockSecond.setText(timestr.split(":")[2])

