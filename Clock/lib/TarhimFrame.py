from PySide2 import QtWidgets, QtGui, QtCore, QtNetwork
from PySide2.QtCore import Qt
from PySide2.QtGui import QColor

class TarhimFrame(QtWidgets.QFrame):
    def __init__(self, parent, config, width, height):

        QtWidgets.QFrame.__init__(self, parent)
        self.setObjectName("tarhimFrame")
        self.setGeometry(0, 0, width, height)
        self.setStyleSheet("#tarhimFrame { background-color: transparent; border-image: url(" +
            config.iqamahbg+") 0 0 0 0 stretch stretch;}")
        self.setVisible(False)
        self.config = config
        self.xscale = float(width) / 1440.0
        self.yscale = float(height) / 900.0

        print("make tarhimCountdown")
        self.tarhimCountdown = QtWidgets.QLabel(self)
        self.tarhimCountdown.setObjectName("tarhimCountdown")
        tarhimCountdownRect = QtCore.QRect(
            width / 2 - height * .4,
            height * .45 - height * .25,
            height * .8,
            height * .8)
        self.tarhimCountdown.setGeometry(tarhimCountdownRect)
        dcolor = QColor(config.digitalcolor5).darker(0).name()
        lcolor = QColor(config.digitalColorCountdownTarhim).lighter(150).name()
        self.tarhimCountdown.setStyleSheet(
            "#tarhimCountdown { background-color: transparent; font-family:sans-serif;" +
            " font-weight: bold; color: " +
            lcolor +
            "; background-color: transparent; font-size: " +
            str(int(config.digitalsizeContdownTarhim * self.xscale)) +
            "px; " +
            config.fontattr +
            "}")
        self.tarhimCountdown.setAlignment(Qt.AlignCenter)
        # tarhimCountdown.setGeometry(clockrect)
        glow = QtWidgets.QGraphicsDropShadowEffect()
        glow.setOffset(0)
        glow.setBlurRadius(50)
        glow.setColor(QColor(dcolor))
        self.tarhimCountdown.setGraphicsEffect(glow)
        print("made tarhimCountdown")
        # Frame 3
        self.datex3 = QtWidgets.QLabel(self)
        self.datex3.setObjectName("datex3")
        self.datex3.setStyleSheet("#datex3 { font-family:sans-serif; color: " +
                             config.textcolor +
                             "; background-color: transparent; font-size: " +
                             str(int(50 * self.xscale)) + "px; " +
                             config.fontattr +
                             "}")
        self.datex3.setAlignment(Qt.AlignHCenter | Qt.AlignTop)
        self.datex3.setGeometry(800 * self.xscale, 780 * self.yscale, 640 * self.xscale, 100)

        self.labels = QtWidgets.QLabel(self)
        self.labels.setObjectName("labels")
        self.labels.setStyleSheet("#labels {background-color: transparent;}")
        pixmap = QtGui.QPixmap(config.attention)
        height_label = 150
        width_label = 150
        self.labels.resize(width_label, height_label)
        self.labels.setPixmap(pixmap.scaled(self.labels.size(), QtCore.Qt.IgnoreAspectRatio))
        self.labels.setGeometry(640 * self.xscale, 100 * self.yscale, 1000 * self.xscale, 500 * self.yscale)

        self.silent = QtWidgets.QLabel(self)
        self.silent.setObjectName("silent")
        self.silent.setStyleSheet("#silent {background-color: transparent;}")
        pixmap = QtGui.QPixmap(config.slash)
        height_label = 150
        width_label = 400
        self.silent.resize(width_label, height_label)
        self.silent.setPixmap(pixmap.scaled(self.silent.size(), QtCore.Qt.IgnoreAspectRatio))
        self.silent.setGeometry(515 * self.xscale, 450 * self.yscale, 1000 * self.xscale, 500 * self.yscale)

    def setCountdown(self, deltastr):
        self.tarhimCountdown.setText(deltastr)
